import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import { AgmCoreModule } from 'angular2-google-maps/core';
import {KeysPipe} from "./home/KeysPipe";
import {ItemComponent} from "./item/item.component";
import { DetailsComponent } from './details/details.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'add-item', component: ItemComponent},
  {path: 'details/:id', component: DetailsComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    KeysPipe,
    ItemComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes, { useHash: false })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
