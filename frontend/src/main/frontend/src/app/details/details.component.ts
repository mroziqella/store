import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ItemService} from "../item/item.service";
import {Observable} from "rxjs/Observable";
import {Item} from "../item/item.add";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  providers:[ItemService]
})
export class DetailsComponent implements OnInit,OnDestroy  {
  private sub: any;
  observableItems:Observable<Item>;
  item:Item;
  constructor(private route: ActivatedRoute,private  _itemService:ItemService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.observableItems=this._itemService.getItem(params['id']);
      this.observableItems.subscribe(item => this.item = item);
    });

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
