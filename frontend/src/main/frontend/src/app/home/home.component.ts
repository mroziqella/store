import {Component, Pipe, PipeTransform, OnInit, Input} from '@angular/core';
import {ItemService} from "../item/item.service";
import {Item} from "../item/item.add";
import {Observable} from "rxjs/Observable";


@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  providers:[ItemService]
})
export class HomeComponent implements OnInit{

  observableItems: Observable<Item[]>;
  items: any;
  observableOpportunity: Observable<Item[]>;
  opportuity:any;
  errorMessage: String;
  page:number = 0;

  constructor(private  _itemService:ItemService) {

  }


  ngOnInit(): void {
    this.observableItems=this._itemService.getItems(this.page);
    this.observableItems.subscribe(
      item => this.items = item,
      error =>  this.errorMessage = <any>error);

    this.observableOpportunity = this._itemService.getItemsOpportunity();

    this.observableOpportunity.subscribe(
      item => this.opportuity = item,
      error =>  this.errorMessage = <any>error);

  }

  change1(next:number){
    this.page=this.page+next;
    this.observableItems=this._itemService.getItems(this.page);
    this.observableItems.subscribe(
      item => this.items = item,
      error =>  this.errorMessage = <any>error);
  }


}

