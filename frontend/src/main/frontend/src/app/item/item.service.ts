import { Injectable } from '@angular/core';
import {Http,Response} from "@angular/http";
import {Item} from "./item.add";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

@Injectable()
export class ItemService {

  http: Http;
  posts_Url: string = 'http://localhost:8080/item';

  constructor(public _http: Http) {
    this.http = _http;
  }

  postItem(item: Item) {
    return this.http.post(this.posts_Url, item, {}).map(res => res.json());
  }

  getItems(page:number): Observable<any> {
    return this.http.get(this.posts_Url+"?page="+page+"&size=5")
      .map(res => res.json());

  }
  getItem(id:String): Observable<Item> {
    return this.http.get(this.posts_Url+"/details/"+id)
      .map(res => res.json());

  }

  getItemsOpportunity(): Observable<any> {
    return this.http.get(this.posts_Url+"/opportunity?page=0&size=3&sort=discount,desc")
      .map(res => res.json());

  }



}

