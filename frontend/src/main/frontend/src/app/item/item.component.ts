import {Component, Input, OnInit} from '@angular/core';
import {Item} from "./item.add";
import {ItemService} from "./item.service";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  providers:[ItemService]
})
export class ItemComponent implements OnInit {
  @Input() item:Item;
  responseStatus:Object= [];
  status:boolean ;
  constructor(private  _itemService:ItemService) {

  }

  submitItem(){
    this._itemService.postItem(this.item).subscribe(
      data => console.log(this.responseStatus = data),
      err => console.log(err),
      () => console.log('Request Completed')
    );
    this.status = true;
  }
  ngOnInit() {
    this.item = new Item();
  }
}
