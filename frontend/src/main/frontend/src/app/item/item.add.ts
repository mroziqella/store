export class Item {
  public  id: string;
  public title: string;
  public description: string;
  public shortDescription: string;
  public type: string;
  public price: number;
  public discount?: number
  public count?: number

}
