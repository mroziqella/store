package pl.store.item.query;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;



public interface ItemBasicRepositoryQuery extends PagingAndSortingRepository<ItemBasic, Long> {

    Page<ItemBasic> findByDiscountNotNull(Pageable pageable);
    ItemBasic findById(String id);

}
