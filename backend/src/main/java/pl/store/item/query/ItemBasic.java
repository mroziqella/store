package pl.store.item.query;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "item")
@Getter
@NoArgsConstructor
public class ItemBasic {
    @Id
    private String id;
    private String title;
    private String shortDescription;
    private String description;
    private String type;
    private String count;
    private Long price;
    private Long discount;

}
