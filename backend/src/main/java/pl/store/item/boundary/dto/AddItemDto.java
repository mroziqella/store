package pl.store.item.boundary.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;

@Getter
@NoArgsConstructor
public class AddItemDto {
    private String id;
    private String title;
    private String description;
    private String shortDescription;
    private String type;
    private String count;
    private Long price;
    private Long discount;
    private Map<String, String> additionalAttribute;
}
