package pl.store.item.boundary.endpoint;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.store.item.boundary.dto.AddItemDto;
import pl.store.item.command.domian.ItemFacade;
import pl.store.item.query.ItemBasicRepositoryQuery;

@RestController
@RequestMapping("/item")
class ItemCommandEndpoint {


    private final ItemFacade itemFacade;
    private final ItemBasicRepositoryQuery itemBasicRepositoryQuery;

    ItemCommandEndpoint(ItemFacade pItemFacade, ItemBasicRepositoryQuery pItemBasicRepositoryQuery) {
        itemFacade = pItemFacade;
        itemBasicRepositoryQuery = pItemBasicRepositoryQuery;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    private void addItem(@RequestBody AddItemDto pAddItemDto) {
        itemFacade.addItem(pAddItemDto);
    }

    @GetMapping
    private ResponseEntity getItems(Pageable pageable) {
        return ResponseEntity.ok().body(itemBasicRepositoryQuery.findAll(pageable));
    }

    @GetMapping("/opportunity")
    private ResponseEntity getOpportunity(Pageable pPageable) {
        return ResponseEntity.ok().body(itemBasicRepositoryQuery.findByDiscountNotNull(pPageable));
    }

    @GetMapping("/details/{id}")
    private ResponseEntity getDetalis(@PathVariable String id) {
        return ResponseEntity.ok().body(itemBasicRepositoryQuery.findById(id));
    }

}
