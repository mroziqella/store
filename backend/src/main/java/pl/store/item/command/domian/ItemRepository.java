package pl.store.item.command.domian;

import org.springframework.data.mongodb.repository.MongoRepository;

interface ItemRepository extends MongoRepository<Item, Long> {
}
