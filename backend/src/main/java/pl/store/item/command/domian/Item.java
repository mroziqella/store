package pl.store.item.command.domian;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "item")
@Getter
@Setter
@NoArgsConstructor
class Item {
    @Id
    private String id;
    private String title;
    private String description;
    private String shortDescription;
    private String type;
    private String count;
    private Long price;
    private Long discount;
    private Map<String, String> additionalAttribute;
}
