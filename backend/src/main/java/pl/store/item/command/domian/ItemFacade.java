package pl.store.item.command.domian;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.store.item.boundary.dto.AddItemDto;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemFacade {

   private final ItemRepository itemRepository;

    private ModelMapper modelMapper = new ModelMapper();


    public void addItem(AddItemDto pAddItemDto){
        Item map = modelMapper.map(pAddItemDto, Item.class);
        itemRepository.insert(map);
    }

}
